//slider section
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}


//input validation section in register page
function validatePhoneNumberForm (){
  String.prototype.toEnglishDigits = function () {
    var charCodeZero = '۰'.charCodeAt(0);
    return parseInt(this.replace(/[۰-۹]/g, function (w) {
        return w.charCodeAt(0) - charCodeZero;
    }));
  }
  var inputValue = document.forms ["phoneNumberForm"]["phoneNumber"].value;
  console.log('inputValue is : ............',inputValue.toEnglishDigits())
  if(inputValue.length !== 11 || isNaN(inputValue.toEnglishDigits())){
    if(parseInt(inputValue[0].toEnglishDigits()) === 9 ){
      return true
    } else {
      alert('شماره وارد شده صحیح نیست')
      return false
    }
  } else if(inputValue.length === 11 && parseInt(inputValue[0].toEnglishDigits()) !== 0) {
    alert('شماره وارد شده صحیح نیست')
    return false
  }
}


//input validation section in confirm page
function validateCodeNumberForm (){
  String.prototype.toEnglishDigits = function () {
    var charCodeZero = '۰'.charCodeAt(0);
    return parseInt(this.replace(/[۰-۹]/g, function (w) {
        return w.charCodeAt(0) - charCodeZero;
    }));
  }
  var inputValue = document.forms ["codeNumberForm"]["codeNumber"].value;
  if(inputValue.length !== 4 || isNaN(inputValue.toEnglishDigits())){
    alert('کد وارد شده صحیح نیست')
    return false
  }
}

//onclick apple icon in index page
function alertFunction(){
  alert('ثبت نام کنید اپ به زودی در دسترس خواهد بود')
}

//input validation section for phoneNumber
function validatePhoneNumberMaxlength(inputValue){
  String.prototype.toEnglishDigits = function () {
    var charCodeZero = '۰'.charCodeAt(0);
    return parseInt(this.replace(/[۰-۹]/g, function (w) {
        return w.charCodeAt(0) - charCodeZero;
    }));
  }
  if(parseInt(inputValue[0].toEnglishDigits()) === 9){
    document.getElementById("inputNums").setAttribute("maxlength", 10);
  } else if(parseInt(inputValue[0].toEnglishDigits()) === 0){
    document.getElementById("inputNums").setAttribute("maxlength", 11);
  }
}




// '۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'