var express = require ('express');
var bodyParser = require ('body-parser');
var path = require ('path');
var fetch = require ('node-fetch');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var app = express();
var mysql = require('mysql');


//INSERT INTO users (DateTime, Ref, IPAddress) VALUES ("2018-07-23", "765", "127.0.0.4")
//test persian numbers ۰۹۹۰۹۰۵۹۴۸۶ ....  ۱۲۳۴۵۶۷۸۰۹

var results = [];

//User Pass For Connect To DB
// var connectionPool = mysql.createPool({
//     connectionLimit: 4,
//     host     : 'localhost',
// 	port     : '3306',
//     user     : 'kaale_LangingDB',
//     password : 'Flb6zusTm%',
//     database : 'kaaleskeh_ir_Landing'
// });


//User Pass For Connect To DB
var connectionPool = mysql.createPool({
    connectionLimit: 4,
    host     : '127.0.0.1',
    user     : 'root',
    password : 'Flb6zusTm%',
    database : 'landing'
});


//const variables
const SEND_OTP_URL ='http://rafshadow.ir'


//Repeatedly Used Functions
String.prototype.toEnglishDigits = function () {
    var charCodeZero = '۰'.charCodeAt(0);
    return parseInt(this.replace(/[۰-۹]/g, function (w) {
        return w.charCodeAt(0) - charCodeZero;
    }));
}

//View Engine
app.set('view engine','ejs')
app.set('views',path.join(__dirname,'views'))


//Parse Request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Set Session & CookieParser
app.use(cookieParser());
app.use(session({secret: "Shh, its a secret!"}));

//Set Static Path
app.use(express.static(path.join(__dirname,'public')));

//download files
app.get('/download/KaaleskehAPK', function(req, res){
    var file = __dirname + `/public/Apps/Kaaleskeh_V_0.1.5.apk`;
    res.download(file);
});
app.get('/download/BabyFace/PictureFirst', function(req, res){
    var file = __dirname + '/public/Apps/BabyFace-PictureFirst-1.1.11.apk';
    res.download(file);
});
app.get('/download/BabyFace/NumberFirst', function(req, res){
    var file = __dirname + '/public/Apps/BabyFace-NumberFirst-1.1.11.apk';
    res.download(file);
});


function getRefAndIPFromDB(req, res, next) {

    connectionPool.query("SELECT * FROM users", function (err, result, fields) {
        if (err) throw err;
        results = [];
        results.push(result)
    });
    setTimeout(function(){next()},2000)
    
}

//Show DB Table Interface
app.get('/analyticReport', getRefAndIPFromDB, function(req, res){
    var count = results[0] ? results[0].length : null
    res.render(__dirname + `/views/analyticReport/tableView`,{
        table : results[0],
        count
    })
});


//Intial Routing Main Pages Section
app.get('/',function(req, res){
    res.render('index');
});

app.get('/register',function(req, res){
    res.render('register')
});

app.get('/apple/registraion',function(req, res){
    setTimeout(function(){
        res.render('register')
    },5000)
});





//MiddleWare Function For Fetch PhoneNumber
function sendPhoneNumberMainPage(req, res, next){
    if(req.body.phoneNumber){
        req.session.phoneNumber = '0' + req.body.phoneNumber.toEnglishDigits()
        console.log('session phoneNumber in register page:',req.session.phoneNumber)
        fetch( SEND_OTP_URL + `/SendOperatorOtpRequest?phone=${req.session.phoneNumber}&serviceId=608&`)
            .then(res => res.json())
            .then(response => {
                if(response === `the phone: ${req.session.phoneNumber} status is registered`){
                    console.log('Delivered PhonNumber From RafShadow:',response)
                    res.render(__dirname + '/views/registeredBefore')
                } else {
                    console.log('Delivered PhonNumber From RafShadow:',response)
                    next();
                }
            })

    }
}
//SendPhoneNumber Routing
app.post('/register/sendPhoneNumber', sendPhoneNumberMainPage ,function(req, res){
    res.render('confirm')
});


//MiddleWare Function For Fetch CodeNumber
function sendCodeNumberMainPage(req, res, next){
    if(req.body.codeNumber){
        if (req.body.codeNumber[0].toEnglishDigits() === 0) {
            var codeNumber = '0' + req.body.codeNumber.toEnglishDigits()
        } else {
            var codeNumber = req.body.codeNumber.toEnglishDigits()
        }
        console.log('Local codeNumber in comfirm page:',codeNumber)
        fetch( SEND_OTP_URL + `/OperatorPinResponse?phone=${req.session.phoneNumber}&serviceId=608&pin=${codeNumber}`)
            .then(res => res.json())
            .then(response => console.log('Delivered CodeNumber From RafShadow:',response))
        next()
    }
}
//SendCodeNumber Routing
app.post('/register/sendCodeNumber', sendCodeNumberMainPage ,function(req, res){
    res.render('join')
});




//Landing Pages Section ..............vvvvv


//just for /landing111 route ... must be remove vvv
app.get('/landing111',function(req, res){
    res.render(__dirname + `/views/landingPages/code111/landing111`)
});//..............................................must be remove^^^



// app.get('/landing/:refNum',function(req, res){
//     res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landing${req.params.refNum}`)
// });

function storRefAndIPInDB (req, res, next) {
    var ip = req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    (req.connection.socket ? req.connection.socket.remoteAddress : null);
    var sql = "INSERT INTO users (id, DateTime, Ref, IPAddress) VALUES ?";
    var values = [
        [0, new Date().getTime(), req.params.refNum, ip]
    ];
    connectionPool.query(sql, [values], function (err, result) {
        if (err) throw err;
        console.log("Number Of Records Inserted: " + result.affectedRows);
    });
    
    next();
}


app.get('/landing/:refNum', storRefAndIPInDB, function(req, res){
    res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landing${req.params.refNum}`,{
        campaignName : null,
        landingCode : req.params.refNum,
    })
});


//MiddleWare Function For Fetch PhoneNumber
function sendPhoneNumberLandingPage(req, res, next){
    if(req.body.phoneNumber){
        var campaignName = req.params.refCamp
        req.session.phoneNumber = '0' + req.body.phoneNumber.toEnglishDigits()
        // if(campaignName === undefined){
        //     refs = req.params.refNum
        // } else {
        //     refs = campaignName + '-' + req.params.refNum
        // }
        console.log('session phoneNumber in registerLanding page:',req.session.phoneNumber)
        console.log('campaignName + refNum:',campaignName+req.params.refNum)
        fetch( SEND_OTP_URL + `/SendOperatorOtpRequest?phone=${req.session.phoneNumber}&serviceId=608&ref=${req.params.refNum}`)
            .then(res => res.json())
            .then(response => {
                if(response === `the phone: ${req.session.phoneNumber} status is registered`){
                    console.log('Delivered PhonNumber From RafShadow:',response)
                    res.render(__dirname + '/views/registeredBefore')
                } else {
                    console.log('Delivered PhonNumber From RafShadow:',response)
                    next();
                }
            })

    }
}


app.post(`/landing/sendPhoneNumber/:refNum`, sendPhoneNumberLandingPage ,function(req, res){
    res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landingConfirm${req.params.refNum}`,{
        campaignName : null,
        landingCode : req.params.refNum
    })
});


//MiddleWare Function For Fetch CodeNumber
function sendCodeNumberLandingPage(req, res, next){
    if(req.body.codeNumber){
        var campaignName = req.params.refCamp;
        // if(campaignName === undefined){
        //     refs = req.params.refNum
        // } else {
        //     refs = campaignName + '-' + req.params.refNum
        // }
        if (req.body.codeNumber[0].toEnglishDigits() === 0) {
            var codeNumber = '0' + req.body.codeNumber.toEnglishDigits()
        } else {
            var codeNumber = req.body.codeNumber.toEnglishDigits()
        }
        console.log('Local codeNumber in comfirmLanding page:',codeNumber)
        console.log('campaignName + refNum:',campaignName+req.params.refNum)
        fetch( SEND_OTP_URL + `/OperatorPinResponse?phone=${req.session.phoneNumber}&serviceId=608&pin=${codeNumber}&ref=${req.params.refNum}`)
            .then(res => res.json())
            .then(response => console.log('Delivered CodeNumber From RafShadow:',response))
        next()
    }
}


app.post('/landing/sendCodeNumber/:refNum', sendCodeNumberLandingPage ,function(req, res){
    res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landingJoin${req.params.refNum}`)
});


//Landings With RefCampaign And RefCod ........................................................................................................


// app.get('/landing/:refCamp/:refNum',function(req, res){
//     //campaignName = req.params.refCamp
//     res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landing${req.params.refNum}`,{
//         campaignName : req.params.refCamp,
//         landingCode : req.params.refNum
//     })
// });


// app.post(`/landing/sendPhoneNumber/:refCamp/:refNum`, sendPhoneNumberLandingPage ,function(req, res){
//     res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landingConfirm${req.params.refNum}`,{
//         campaignName : req.params.refCamp,
//         landingCode : req.params.refNum
//     })
// });


//app.post('/landing/sendCodeNumber/:refCamp/:refNum', sendCodeNumberLandingPage ,function(req, res){
//    res.render(__dirname + `/views/landingPages/code${req.params.refNum}/landingJoin${req.params.refNum}`)
//});


//.................................................................................................................................................



//listen port
app.listen(3000,function(){
    console.log('server runs on port 3000');
});