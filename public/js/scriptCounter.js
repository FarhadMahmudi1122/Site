var before = new Date() ;
var minuteNumber = 5 ;
var interval = setInterval( function () {
    var counter = ((minuteNumber * 60 * 1001) - (new Date().getTime() - before.getTime())).toString().slice(0,-3) ;
    var newMinnets = ((counter / 60).toString())[0] ;
    var newSeconds = (counter - (newMinnets * 60)).toString() ;
    if(newSeconds<10){
        document.getElementById('counter').innerHTML = '0' + newMinnets + ":" + '0' + newSeconds ;
        
    } else {
        document.getElementById('counter').innerHTML = '0' + newMinnets + ":" + newSeconds ;        
    }
    if(newMinnets == 0 && newSeconds == 0 ){
        clearInterval(interval)        
        document.getElementById('counter').innerHTML = "00" + ":" + "00" ;
        document.getElementById('reTryBTN').setAttribute("href","javascript:history.back()") ;             
    }
} , 1000 );


(function (global) { 
    
    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }
    
})(window);